package org.example.sudoku;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ResultActivity extends Fragment implements OnClickListener{

	public Button bSend, bTakePhoto, bBackToMenu;
	public TextView tName, tScore;
	
	static String username;
	static int score;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String POST_COMMENT_URL = "http://192.168.56.1/sudoku/addscore.php"; 

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        
		if (container == null) {
	        return null;
	    }
		
		View view = inflater.inflate(R.layout.result, container, false);

		//get score from bundle
		/*
        Bundle b = getIntent().getExtras();
		score= b.getInt("score");
				*/
        score = 100;
		tScore = (TextView)view.findViewById(R.id.textView4);
		tScore.setText(String.valueOf(score)); 

        
	    //get username from sharedPreff
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		username = sp.getString("username", ""); 
		tName = (TextView)view.findViewById(R.id.textView2);
		if(username !=null) {
			tName.setText(username);
		}
		
		//set buttons
		bSend = (Button)view.findViewById(R.id.button1);
		bTakePhoto = (Button)view.findViewById(R.id.button2);		
		bBackToMenu = (Button)view.findViewById(R.id.button3);
		bSend.setOnClickListener(this);
		bTakePhoto.setOnClickListener(this);
		bBackToMenu.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1: //add score
			new AddScore().execute();
			break;
		case R.id.button2: //take a photo
           /*
			Intent intent = new Intent(getActivity(),CamTestActivity.class);
            startActivity(intent);
			//finish();*/
			
			break;
		case R.id.button3: //go to menu
			/*
			 Intent i = new Intent(getActivity(), MainActivity.class);
			
			startActivity(i); */
			//finish();
			getActivity().getFragmentManager().beginTransaction().remove(this).commit();
			Intent i = new Intent(getActivity(), MainActivity.class);
			startActivity(i);
			break;
			
		default:
			break;
		}		
	}
		

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	class AddScore extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading score...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub

            int success;
            String username = tName.getText().toString();
            String score = tScore.getText().toString();
            try {
                // Building Parameters
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username", username));
                params.add(new BasicNameValuePair("score", score));

                Log.d("request!", "starting");

                //Posting user data to script
                JSONObject json = jsonParser.makeHttpRequest(
                		POST_COMMENT_URL, "POST", params);

                // full json response
                Log.d("Login attempt", json.toString());

                // json success element
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                	Log.d("Points added", json.toString());
                	//finish();
                	return json.getString(TAG_MESSAGE);
                }else{
                	Log.d("Points Failure!", json.getString(TAG_MESSAGE));
                	return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
		}
		/**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();
            if (file_url != null){
            	/**
				Context context = getActivity().getApplicationContext();//new code
				Toast.makeText(context, file_url, Toast.LENGTH_LONG).show();
				*/
            }
        }
	}
}
