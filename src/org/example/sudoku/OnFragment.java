package org.example.sudoku;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

@SuppressLint("NewApi")
public class OnFragment extends Fragment {

    public Button BSimple; 
    public Button BMulti; 
    public Button BRanking;
    public Button BAbout;
    public Button BExit;
    OnClick iStart;

    @SuppressLint("NewApi")
	interface OnClick{
        public void CSimple();
        public void CMulti();
        public void CRanking();
        public void CAbout();
        public void CExit();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public OnFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            iStart = (OnClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);

        BSimple = (Button)view.findViewById(R.id.new_button);
        BSimple.setOnClickListener(simpleClickListener);

        BMulti = (Button)view.findViewById(R.id.multiplayer);
        BMulti.setOnClickListener(multiClickListener);
        
        BRanking = (Button)view.findViewById(R.id.ranking_button);
        BRanking.setOnClickListener(rankingClickListener);

        BAbout = (Button)view.findViewById(R.id.about_button);
        BAbout.setOnClickListener(aboutClickListener);

        BExit = (Button)view.findViewById(R.id.exit_button);
        BExit.setOnClickListener(exitClickListener);

        return view;
    }

    public View.OnClickListener simpleClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view){
            iStart.CSimple();
        }
    };
    public View.OnClickListener multiClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view){
            iStart.CMulti();
        }
    };
    public View.OnClickListener rankingClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view){
            iStart.CRanking();
        }
    };
    public View.OnClickListener aboutClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view){
            iStart.CAbout();
        }
    };
    public View.OnClickListener exitClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view){
            iStart.CExit();
        }
    };
}