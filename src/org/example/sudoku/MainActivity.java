package org.example.sudoku;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class MainActivity extends Activity implements  OnFragment.OnClick {

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my);//activity_main
		
        if (savedInstanceState == null) {
            OnFragment startFragment = new OnFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, startFragment) 																																																																																																																																																																																																																																																																																									
                    .commit();
        }
	}
	
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
		
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
		startActivity(new Intent(this, Prefs. class));
		return true;
		// More items go here (if any) ...

		}
		return false;
	}
	
	@Override
	public void CSimple() {
		// TODO Auto-generated method stub
	
		getFragmentManager().beginTransaction().replace(R.id.container, new Game()).commit();
		
/*		
		Game gameFragment = new Game();
		
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.container, gameFragment);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
*/		
	}

	@Override
	public void CMulti() {
		// TODO Auto-generated method stub
        Login loginFragment = new Login();
        
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, loginFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
      
	}

	@Override
	public void CRanking() {
		// TODO Auto-generated method stub
        Ranking rankingFragment = new Ranking();
        
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, rankingFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
	}

	@Override
	public void CAbout() {
		// TODO Auto-generated method stub
        About aboutFragment = new About();
        
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, aboutFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
	}

	@Override
	public void CExit() {
		// TODO Auto-generated method stub
		finish();
	}
}
