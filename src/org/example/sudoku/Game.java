package org.example.sudoku;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//public class Game extends Activity {
public class Game extends Fragment {
   private static final String TAG = "Sudoku";

   public static final String KEY_DIFFICULTY = "org.example.sudoku.difficulty";
   
   public static final int DIFFICULTY_EASY = 0;
   public static final int DIFFICULTY_MEDIUM = 1;
   public static final int DIFFICULTY_HARD = 2;
   
   int score = 100;
   public static int multigameflag;
   public static String flaga = "";
   
   private int puzzle[];
   private final String easyPuzzle =
      "362581479914230856785694231" +
      "179462583823759614546813927" +
      "431925768657148392298376145";
   private final String mediumPuzzle =
      "650000070000506000014000005" +
      "007009000002314700000700800" +
      "500000630000201000030000097";
   private final String hardPuzzle =
      "009000000080605020501078000" +
      "000000700706040102004000000" +
      "000720903090301080000000600";

   //private PuzzleView puzzleView;
   public PuzzleView puzzleView;

   @Override
   /*protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Log.d(TAG, "onCreate");
*/
	 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
	  /**FRAGMENT*
	   PuzzleView pw = new PuzzleView(getActivity());
       View view = inflater.inflate(R.layout.result,
               container, false);
      ***/ 
      //ACTIVITY
	  //int diff = getIntent().getIntExtra(KEY_DIFFICULTY, DIFFICULTY_EASY);
      //puzzle = getPuzzle(diff);
      puzzle = fromPuzzleString(easyPuzzle);
      calculateUsedTiles();
      // puzzleView = new PuzzleView(this);
      puzzleView = new PuzzleView(getActivity(), this); // getApplicationContext()
      //setContentView(puzzleView);
      //puzzleView.requestFocus();
      return puzzleView;
   }

   /* Given a difficulty level, come up with a new puzzle */
   private int[] getPuzzle(int diff) {
      String puz;
	  
      // TODO: Continue last game
      switch (diff) {
      case DIFFICULTY_HARD:
         puz = hardPuzzle;
         break;
      case DIFFICULTY_MEDIUM:
         puz = mediumPuzzle;
         break;
      case DIFFICULTY_EASY:
      default:
         puz = easyPuzzle;
         break;
      }
	  
      return fromPuzzleString(puz);
   }

   /** Convert an array into a puzzle string */
   static private String toPuzzleString(int[] puz) {
      StringBuilder buf = new StringBuilder();
      for (int element : puz) {
         buf.append(element);
      }
      return buf.toString();
   }
  
   /** Convert a puzzle string into an array */
   static protected int[] fromPuzzleString(String string) {
      int[] puz = new int[string.length()];
      for (int i = 0; i < puz.length; i++) {
         puz[i] = string.charAt(i) - '0';
      }
      return puz;
   }

   /** Return the tile at the given coordinates */
   private int getTile(int x, int y) {
      return puzzle[y * 9 + x];
   }
   /** Change the tile at the given coordinates */

   private void setTile(int x, int y, int value) {
      puzzle[y * 9 + x] = value;
   }

   /** Return a string for the tile at the given coordinates */
   protected String getTileString(int x, int y) {
      int v = getTile(x, y);
      if (v == 0)
         return "";
      else
         return String.valueOf(v);
   }

   /** Change the tile only if it's a valid move */
   protected boolean setTileIfValid(int x, int y, int value) {
      int tiles[] = getUsedTiles(x, y);
      if (value != 0) {
         for (int tile : tiles) {
            if (tile == value)
               return false;
         }
      }
      setTile(x, y, value);
      calculateUsedTiles();
      return true;
   }

   /** Open the keypad if there are any valid moves */
   protected void showKeypadOrError(int x, int y) {
      int tiles[] = getUsedTiles(x, y);
      if (tiles.length == 9) {
         //Toast toast = Toast.makeText(this, R.string.no_moves_label, Toast.LENGTH_SHORT);
			Context context = getActivity().getApplicationContext();//new code
			Toast.makeText(context, R.string.no_moves_label, Toast.LENGTH_LONG).show();
        // toast.setGravity(Gravity.CENTER, 0, 0);
         //toast.show();
      } else {
         Log.d(TAG, "showKeypad: used=" + toPuzzleString(tiles));
         //Dialog v = new Keypad(this, tiles, puzzleView);
         Dialog v = new Keypad(getActivity(), tiles, puzzleView);
         v.show();
      }
   }

   /** Cache of used tiles */
   private final int used[][][] = new int[9][9][];

   /** Return cached used tiles visible from the given coords */
   protected int[] getUsedTiles(int x, int y) {
      return used[x][y];
   }

   /** Compute the two dimensional array of used tiles */
   private void calculateUsedTiles() {
      for (int x = 0; x < 9; x++) {
         for (int y = 0; y < 9; y++) {
            used[x][y] = calculateUsedTiles(x, y);
            // Log.d(TAG, "used[" + x + "][" + y + "] = "
            // + toPuzzleString(used[x][y]));
         }
      }
   }

   /** Compute the used tiles visible from this position */
   private int[] calculateUsedTiles(int x, int y) {
      int c[] = new int[9];
      // horizontal
      for (int i = 0; i < 9; i++) { 
         if (i == x)
            continue;
         int t = getTile(i, y);
         if (t != 0)
            c[t - 1] = t;
      }
      // vertical
      for (int i = 0; i < 9; i++) { 
         if (i == y)
            continue;
         int t = getTile(x, i);
         if (t != 0)
            c[t - 1] = t;
      }
      // same cell block
      int startx = (x / 3) * 3; 
      int starty = (y / 3) * 3;
      for (int i = startx; i < startx + 3; i++) {
         for (int j = starty; j < starty + 3; j++) {
            if (i == x && j == y)
               continue;
            int t = getTile(i, j);
            if (t != 0)
               c[t - 1] = t;
         }
      }
      // compress
      int nused = 0; 
      for (int t : c) {
         if (t != 0)
            nused++;
      }
      int c1[] = new int[nused];
      nused = 0;
      for (int t : c) {
         if (t != 0)
            c1[nused++] = t;
      }
      return c1;
   }
   
   /****** Check to see if the game is complete **/
   public boolean isSolved() {

       for (int element : puzzle) {
    	   if (element == 0) return false;
    	}
    	return true;
   }
   
   @SuppressLint("NewApi")
   @TargetApi(Build.VERSION_CODES.HONEYCOMB)
   public boolean checkIsSolved()
   {
		//Bundle loginbundle = this.getArguments();
		//multigameflag= loginbundle.getInt("multigameflag");
		//int multigameflag = 1; 
	   
	    
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		flaga = sp.getString("flaga", ""); 
		
       //check if the game is complete after each valid move
		if (isSolved() == true) {    	
    	   if (flaga == "multiflag"){ //if user started game from multimenu
				/******** Bundle *******/
    		   /* 
    		   Intent intent = new Intent(Game.this, ResultActivity.class);
				Bundle b = new Bundle();
				b.putInt("score", score); //Your score
				intent.putExtras(b); //Put your score to your next Intent
				startActivity(intent);
				finish();
				*/

    		   getFragmentManager().beginTransaction().replace(R.id.container, new ResultActivity()).commit();
   	     
				/******** Shared Pref *******/
				/*
    		    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(Game.this); //Login.this
				Editor edit = sp.edit();
				edit.putInt("score", score);
				edit.commit();				
				*/
		        
    	   }
    	   else{ //if user choosen singleplayer

				Context context = getActivity().getApplicationContext();//new code
				Toast.makeText(context, "KONIEC", Toast.LENGTH_LONG).show();
    	   }
       } 
       else
       {
           return false;
       }
		
       return false;
   }
}
 