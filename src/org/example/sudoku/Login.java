package org.example.sudoku;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Login extends Fragment implements OnClickListener {

	public EditText user, pass;
	public Button mSubmit, mRegister;

	// Progress Dialog
	private ProgressDialog pDialog;

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	private static final String LOGIN_URL = "http://192.168.56.1/sudoku/login.php";

	// JSON element ids from repsonse of php script:
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_MESSAGE = "message";
	
	//Flag for next activity - game. It is sending via bundle
    int multigameflag = 1; 
    
	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		if (container == null) {
	        return null;
	    }
		
        View view = inflater.inflate(R.layout.login,
                container, false);
        
		// setup input fields
		user = (EditText)view.findViewById(R.id.username);
		pass = (EditText)view.findViewById(R.id.password);
		
		// setup buttons
		mSubmit = (Button) view.findViewById(R.id.login);
		mRegister = (Button) view.findViewById(R.id.register);

		// register listeners
		mSubmit.setOnClickListener(this);
		mRegister.setOnClickListener(this);
		
        return view;
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.login:
			new AttemptLogin().execute();
			break;
		case R.id.register:
			//Intent i = new Intent(this, Register.class);
			//startActivity(i);
			
	        Register registerFragment = new Register();
	        
	        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
	        fragmentTransaction.replace(R.id.container, registerFragment);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
			break;

		default:
			break;
		}
	}

	class AttemptLogin extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(getActivity());//Login.this
			pDialog.setMessage("Attempting login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag
			int success;
			String username = user.getText().toString();
			String password = pass.getText().toString();
			try {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("username", username));
				params.add(new BasicNameValuePair("password", password));

				Log.d("request!", "starting");
				// getting product details by making HTTP request
				JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST",
						params);

				// check your log for json response
				Log.d("Login attempt", json.toString());

				// json success tag
				success = json.getInt(TAG_SUCCESS);
				//if (success == 1) {
					Log.d("Login Successful!", json.toString());
					// save user data
					SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity()); //Login.this
					Editor edit = sp.edit();
					edit.putString("username", username);
					edit.commit();
					
					String multiflag = "multiflag";
					SharedPreferences sp2 = PreferenceManager.getDefaultSharedPreferences(getActivity()); //Login.this
					Editor edit2 = sp2.edit();
					edit2.putString("multiflag", multiflag);
					edit2.commit();
					/*Intent i = new Intent(getActivity(), Game.class);					
					Bundle b = new Bundle();
					b.putInt("multigameflag", multigameflag); 
					i.putExtras(b); //put this information into game activity
					getActivity().finish(); //finish fragment
					startActivity(i);*/
					
					getFragmentManager().beginTransaction().replace(R.id.container, new Game()).commit();
					
					
					return json.getString(TAG_MESSAGE);
					
				//} else {					
				//	Log.d("Login Failure!", json.getString(TAG_MESSAGE));
				//	return json.getString(TAG_MESSAGE);
				//}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();
			if (file_url != null) {
				Context context = getActivity().getApplicationContext();//new code
				Toast.makeText(context, file_url, Toast.LENGTH_LONG).show();
			}
		}
	}
}
